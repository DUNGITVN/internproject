<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/header_footer.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container">
      <div class="footer">
        <div class="row">
          <div class="col-lg-2">
            <img src="../public/image/image_dev_tu/Logo.png">
          </div>
          <div class="col-lg-5">
            <div class="introduce">
              <p><b>Cơ quan chủ quản: Bộ Thông tin và Truyền thông</b></p>
              <p>Số giấy phép: 09/GP - BTTTT, cấp ngày 26/02/2020</p>
              <p><b>Tổng biên tập: </b><span>Nguyễn Anh Tú</span></p>
              <p>Tòa soạn: Số 56, ngách 15, hẻm 15/21, ngõ Thổ Quan, Khâm Thiên, Hà Nội</p>
              <p><b>© 1997 Báo VietNamNet. All rights reserved.</b></p>
              <p><i>Chỉ được phát hành lại thông tin từ website này khi có sự đồng ý bằng văn bản của báo VietNamNet.</i></p>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="advertisement">
                <p><b>LIÊN HỆ QUẢNG CÁO</b></p>
                <p><b> Hà Nội. Hotline:</b> 0373228380 | Email: tu240898@gmail.com</p>
                <p><b>Tp.HCM. Hotline:</b> 0774358308 | Email: vietnamnetjsc.hcm@vietnamnet.vn</p>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>