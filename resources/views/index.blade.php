<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../public/css/style.css">
    <link rel="stylesheet" href="../public/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../public/css/owl.theme.default.min.css">
    <script src="js/owl.carousel.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>
</head>
<body>
    <section class="header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="logo">
                            <img src="image/logo-hanoitourist.jpg">
                        </div>
                        <div class="Dulich0">
                           <ul>
                                <li><a href="#">Du lịch Châu Âu</a></li>
                                <li><a href="#">Du lịch Hàn Quốc</a></li>
                                <li><a href="#">Tour cao cấp</a></li>
                                <li><a href="#">Tour khuyến mại</a></li>
                            </ul>
                        </div>
                        <div class="Header">
                          <div class="header-right">
                            <a href="#">Giới thiệu</a>
                            <a href="#">Liên hệ</a> 
                          </div>
                          <div class="Mail">
                            <p><a href="#"><img src="../public/image/1.png">0904 342 888 - 0904 929 252</a></p>
                            <span><a href="#"><img src="../public/image/mail.png">info@hanoitourist-travel.com</a></span>
                          </div>
                        </div>   
                    </div>
                </div>
            </div>
    </section>
    <section class="menu">
        <nav class="navbar navbar-expand-lg  navbar-dark">
            <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link" href="#">TRANG CHỦ</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">DU LỊCH TRONG NƯỚC</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">DU LỊCH NƯỚC NGOÀI</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="#">HOT TOUR</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="#">COMBO DU LỊCH</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="#">VISA</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="#">VẬN CHUYỂN</a>
                    </li>
                     <li class="nav-item">
                      <a class="nav-link" href="#">THƯ VIỆN</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">TIN TỨC</a>
                    </li>    
                    <li class="x nav-item">             
                        <a class="nav-link" href="#">GIỚI THIỆU</a>
                    </li>
                    <li class="y nav-item">
                        <a class="nav-link" href="#">LIÊN HỆ</a>
                    </li>
                    <li class="abc nav-item">
                        <a class="nav-link" href="#"><img src="image/1.png"> 
                            0904 342 888 - 0904 929 252
                        </a>
                    </li>
                    <li class="bcd nav-item">
                        <a class="nav-link" href="#"><img src="image/mail.png">                    
                            info@hanoitourist-travel.com
                        </a>
                    </li>
                </ul>
            </div>
            </div>
      </nav>
    </section>
    <section class="menu2">
        <img src="image/hero-banner.jpg">     
           <div class="Menu2">
                <div class="container">    
                    <div class="timkiem">
                        <div class="timkiemtour">
                            <span>TÌM KIẾM TOUR</span>
                            <div class="Underline"></div>
                        </div>
                        <div class="ditour">
                            <input type="radio" name="chon"><span>Tour Trong Nước</span>
                            <input type="radio" name="chon"><span>Tour Trong Nước</span>
                        </div>
                        <div class="tim">
                            <div class="timkiemnangcao">
                                <p>Điểm khởi hành</p> 
                                <div class="a">
                                    <img src="image/blue.png">
                                </div>                           
                            </div>
                            <div class="timkiemnangcao">
                                <p>Điểm đến</p> 
                                <div class="a">
                                    <img src="image/blue.png">
                                </div>                           
                            </div>
                            <div class="timkiemnangcao">
                                <p>Lịch trình</p> 
                                <div class="a">
                                    <img src="image/1-copy-3.png">
                                </div>                           
                            </div>
                            <div class="timkiemnangcao">
                                <p>Ngày khởi hành</p> 
                                <div class="a">
                                    <img src="image/1-copy.png">
                                </div>                           
                            </div>
                            <div class="timkiemnangcao">
                                <p>Khoảng giá</p> 
                                <div class="a">
                                    <img src="image/money.png">
                                </div>                           
                            </div>
                            <div class="nut">
                                <button type="button" class="btn">Tìm kiếm</button>
                            </div>
                        </div>  
                    </div>
                </div>  
           </div>
    </section>
    <section class="travel0">
        <div class="Travel0">
            <p class="tieude">DỊCH VỤ CỦA CHÚNG TÔI</p>
            <div class="dv-dichvu">
                <div class="container d-flex">
                    <div class="chondichvu">
                        <div class="cacdichvu">
                            <img src="image/visa.png">
                            <div class="visa">
                                <p>VISA</p>
                            </div>   
                            <div class="ghichudichvu">
                                <span>
                                    Hỗ trợ thủ tục đơn giản. Tỷ lệ thành công 99%. Miễn phí dịch hồ sơ
                                </span>
                                    </div>
                            <div class="linkdichvu">
                                <p><a href="">Xem thêm</a></p>
                            </div>   
                        </div>
                    </div>
                    <div class="chondichvu">
                        <div class="cacdichvu">
                            <img src="image/visa.png">
                            <div class="visa">
                                <p>VISA</p>
                            </div>   
                            <div class="ghichudichvu">
                                <span>
                                    Hỗ trợ thủ tục đơn giản. Tỷ lệ thành công 99%. Miễn phí dịch hồ sơ
                                </span>
                                    </div>
                            <div class="linkdichvu">
                                <p><a href="">Xem thêm</a></p>
                            </div>   
                        </div>
                    </div>
                    <div class="chondichvu">
                        <div class="cacdichvu">
                            <img src="image/visa.png">
                            <div class="visa">
                                <p>VISA</p>
                            </div>   
                            <div class="ghichudichvu">
                                <span>
                                    Hỗ trợ thủ tục đơn giản. Tỷ lệ thành công 99%. Miễn phí dịch hồ sơ
                                </span>
                                    </div>
                            <div class="linkdichvu">
                                <p><a href="">Xem thêm</a></p>
                            </div>   
                        </div>
                    </div>
                    <div class="chondichvu">
                        <div class="cacdichvu">
                            <img src="image/visa.png">
                            <div class="visa">
                                <p>VISA</p>
                            </div>   
                            <div class="ghichudichvu">
                                <span>
                                    Hỗ trợ thủ tục đơn giản. Tỷ lệ thành công 99%. Miễn phí dịch hồ sơ
                                </span>
                                    </div>
                            <div class="linkdichvu">
                                <p><a href="">Xem thêm</a></p>
                            </div>   
                        </div>
                    </div>                     
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="hottour">
                    <span>HOT TOUR</span>
                    <div class="Underline1"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="khuyenmaitour">
                    <ul class="nav nav-pills nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="#home">Tour Giờ Chót</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#menu1">Tour Khuyến Mại</a>
                        </li>
                    </ul>
               
                </div>  
            </div>
        </div>
    </div>
    <section class="class menu3">
        <div class="tab-content">
            <div id="home" class="tab-pane active "><br>  
                <div class="chiaanh">
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>                               
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div id="menu1" class="tab-pane fade "><br>  
                <div class="chiaanh">
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/nhacodai2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                                <div class="ngay">
                                     <p>3N2Đ</p>
                                     <p>16 chỗ</p>
                                </div>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>
    <div class="container">
        <div class="Xemthem">
            <button type="button" class="btn">Xem thêm</button>
        </div>  
    </div>  
    <div class="container">
       <div class="row">
           <div class="col-lg-12">
                <div class="hottour">
                    <span>TOUR TRONG NƯỚC</span>
                    <div class="Underline1"></div>
                    <div class="Underline2">
                        <div class="diemdennoibat">Điểm đến nổi bật</div>
                    </div>
                </div> 
                <div class="text">
                    Tour du lịch trong nước của HanoiTourist đưa bạn đến các điểm tham quan thú vị trải dài tên khắp đất nước Việt Nam. Cùng với đội ngũ nhân viên nhiệt tình, giàu kinh nghiệm, chúng tôi cung cấp các chương trình tour đa dạng, phong phú, khởi hành hàng ngày đáp ứng đầy đủ nhu cầu của quý khách hàng với mức giá cạnh tranh nhất!
                </div>   
           </div>
       </div>
    </div>
    <section class="choose">
        <div class="owl-carousel owl-theme">
           <div class="item"><a href="#"><img src="image/HaNoi.png" alt=""><p>HÀ NỘI</p></a></div>
            <div class="item"><a href="#"><img src="image/PhuQuoc.png" alt=""><p>PHÚ QUỐC</p></a></div>
            <div class="item"><a href="#"><img src="image/HoChiMinh.png" alt=""><p>HỒ CHÍ MINH</p></a></div>
            <div class="item"><a href="#"><img src="image/Sapa.png" alt=""><p>SAPA</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
        </div>
        <script>
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                dots: false,
                navText: ["<img src='image/left.png'>","<img src='image/right.png'>" ],
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:7
                    }
                }
            })
        </script>  
        <div class="container">
            <div class="xemtatca">
              <span><a href="#">Xem tất cả các điểm</a>
                <a href="#"><img src="image/next-2.png"></a>
              </span>
            </div>
        </div>
    </section>
    <div class="container">
       <div class="row">
           <div class="col-lg-6">
                <div class="hottour">
                    <div class="Underline2">
                        <div class="diemdennoibat">TOUR NỔI BẬT</div>
                    </div>
                </div> 
           </div>
            <div class="col-lg-6">
               <div class="tournoibat">
                  <ul class="nav nav-pills nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link" href="#a">Tour Theo Điểm Đến</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#b">Tour Theo Dịp</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#c">Tour Thường Xuyên</a>
                    </li>
                </ul>
               </div>
            </div>
       </div>
    </div>
    <section class="class menu4">
            <div class="chiaanh">
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>                               
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <div class="container">
        <div class="Xemthem">
            <button type="button" class="btn">Xem thêm</button>
        </div>  
    </div>  
    <div class="container">
       <div class="row">
           <div class="col-lg-12">
                <div class="hottour">
                    <span>TOUR NƯỚC NGOÀI</span>
                    <div class="Underline1"></div>
                    <div class="Underline2">
                        <div class="diemdennoibat">Điểm đến nổi bật</div>
                    </div>
                </div> 
                <div class="text">
                    Tour du lịch trong nước của HanoiTourist đưa bạn đến các điểm tham quan thú vị trải dài tên khắp đất nước Việt Nam. Cùng với đội ngũ nhân viên nhiệt tình, giàu kinh nghiệm, chúng tôi cung cấp các chương trình tour đa dạng, phong phú, khởi hành hàng ngày đáp ứng đầy đủ nhu cầu của quý khách hàng với mức giá cạnh tranh nhất!
                </div>   
           </div>
       </div>
    </div>
    <section class="choose">
        <div class="owl-carousel owl-theme">
           <div class="item"><a href="#"><img src="image/HaNoi.png" alt=""><p>HÀ NỘI</p></a></div>
            <div class="item"><a href="#"><img src="image/PhuQuoc.png" alt=""><p>PHÚ QUỐC</p></a></div>
            <div class="item"><a href="#"><img src="image/HoChiMinh.png" alt=""><p>HỒ CHÍ MINH</p></a></div>
            <div class="item"><a href="#"><img src="image/Sapa.png" alt=""><p>SAPA</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
            <div class="item"><a href="#"><img src="image/Danang.png" alt=""><p>ĐÀ NẴNG</p></a></div>
        </div>
        <script>
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                dots: false,
                navText: ["<img src='image/left.png'>","<img src='image/right.png'>" ],
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:7
                    }
                }
            })
        </script>  
        <div class="container">
            <div class="xemtatca">
              <span><a href="#">Xem tất cả các điểm</a>
                <a href="#"><img src="image/next-2.png"></a>
              </span>
            </div>
        </div>
    </section>
    <div class="container">
       <div class="row">
           <div class="col-lg-6">
                <div class="hottour">
                    <div class="Underline2">
                        <div class="diemdennoibat">TOUR NỔI BẬT</div>
                    </div>
                </div> 
           </div>
            <div class="col-lg-6">
                <div class="khuyenmaitour">
                    <ul class="nav nav-pills nav-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="#d">Tour Theo Dịp</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#e">Tour Thường Xuyên</a>
                        </li>
                    </ul> 
                    <script>
                        $(document).ready(function(){
                          $(".nav-tabs a").click(function(){
                            $(this).tab('show');
                          });
                          $('.nav-tabs a').on('shown.bs.tab', function(event){
                            var x = $(event.target).text();         // active tab
                            var y = $(event.relatedTarget).text();  // previous tab
                            $(".act span").text(x);
                            $(".prev span").text(y);
                          });
                        });
                    </script>
               </div>  
            </div>
       </div>
    </div>
    <section class="menu5">
            <div class="chiaanh">
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>                               
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tourdulich">
                        <div class="anhdulich">
                            <div class="item">
                                <img src="image/img-copy-2.jpg">
                            </div>     
                            <div class="item1">
                                <img src="image/bg.png">
                                <div class="clock">
                                    <p>Hết hạn 10 Ngày 05:30:12”</p>
                                </div>
                            </div>
                            <div class="ngay">
                                <p>3N2Đ</p>
                                <p>16 chỗ</p>
                            </div>
                        </div>
                        <div class="dulich">
                            <div class="khoihanh">
                                <p>Tour Du Lịch: Hà Nội | Tràng An | Hạ Long | Sapa | Fansipan (6N5Đ) </p>
                            </div>
                            <div class="d-flex">
                                <div class="travel">
                                    <p>Charter Vietnam Airlines</p>
                                    <p>20/3 - 14/4 - 15/4 - 27/4 - 1/6 …</p>
                                </div>
                                <div class="gia">
                                    <p>Giá từ</p>
                                    <p>99,999,999đ</p>
                                    <button type="button" class="btn"><span>Giữ chỗ<span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <div class="container">
        <div class="Xemthem">
            <button type="button" class="btn">Xem thêm</button>
        </div>  
    </div>  
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="hottour">
                    <span>COMBO</span>
                    <div class="Underline1"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="khuyenmaitour">
                    <ul class="nav nav-pills nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link" href="#f">Combo</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#g">Voucher</a>
                    </li>
                </ul>
                <script>
                    $(document).ready(function(){
                      $(".nav-tabs a").click(function(){
                        $(this).tab('show');
                      });
                      $('.nav-tabs a').on('shown.bs.tab', function(event){
                        var x = $(event.target).text();         // active tab
                        var y = $(event.relatedTarget).text();  // previous tab
                        $(".act span").text(x);
                        $(".prev span").text(y);
                      });
                    });
                </script>
                </div>     
            </div>
        </div>
    </div>
    <section class="menu6">
        <div class="chiacombo">
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="Combodulich">
                <div class="combo">
                    <img src="image/combodulich1.jpg">
                </div>
                <div class="anhcombo">
                    <img src="image/bg-1.png">
                    <p>Combo</p>
                </div>
                <div class="combodulich">
                    <div class="goicombo">
                         <span>COMBO HÀ NỘI- NHA TRANG 3N2Đ - PALM BEACH HOTEL TRỌN GÓI</span>
                    </div>
                    <div class="d-flex">
                        <div class="vemaybay">
                            <p>Vé máy bay + Khách sạn 3 sao + Ăn sáng Free + Xe đưa đón 2 chiều </p>
                            <p>2 Ngày 1 Đêm</p>
                            <p>Tháng 6 - 7 - 8 - 9 - 10 - 11</p>
                        </div>
                        <div class="Tongchiphi">
                            <p>Giá từ</p>
                            <p>99,999,999đ</p>
                            <button type="button" class="btn"><span>Giữ chỗ<span></button>
                        </div>
                    </div>
                </div>
            </div>             
        </div>
    </section>
    <div class="container">
        <div class="Xemthem">
            <button type="button" class="btn">Xem thêm</button>
        </div>  
    </div> 
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="hottour">
                    <span>CẨM NANG DU LỊCH</span>
                    <div class="Underline1"></div>
                </div>
                <div class="Underline2">
                    <div class="tintucnoibat">TIN TỨC NỔI BẬT</div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="Underline3">
                    <div class="kinhnghiemdulich">KINH NGHIỆM DU LỊCH</div>
                </div>
            </div>
        </div>
    </div>
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="slider">
                        <div> 
                            <img src="image/Travel1.jpg">   
                            <div class="daude">
                                <span>Đến Phú Quốc nên tham quan những đảo nào ?</span>
                            </div>        
                            <div class="camnang">
                                <a href="#">Cẩm nang du lịch</a>
                            </div>
                            <div class="ngaydangbai">
                                 <span>20/05/2019 | 05:15 &#9679; 2000 lượt xem </span>
                            </div>              
                        </div>
                        <div>
                            <img src="image/Travel1.jpg">   
                            <div class="daude">
                                <span>Đến Phú Quốc nên tham quan những đảo nào ?</span>
                            </div>        
                            <div class="camnang">
                                <a href="#">Cẩm nang du lịch</a>
                            </div>
                            <div class="ngaydangbai">
                                 <span>19/05/2019 | 05:15 &#9679; 2000 lượt xem </span>
                            </div>              
                        </div>
                       <div>
                            <img src="image/Travel1.jpg">   
                            <div class="daude">
                                <span>Đến Phú Quốc nên tham quan những đảo nào ?</span>
                            </div>        
                            <div class="camnang">
                                <a href="#">Cẩm nang du lịch</a>
                            </div>
                            <div class="ngaydangbai">
                                 <span>30/01/2019 | 05:15 &#9679; 2000 lượt xem </span>
                            </div>              
                        </div>
                        <div>
                            <img src="image/Travel1.jpg">   
                            <div class="daude">
                                <span>Đến Phú Quốc nên tham quan những đảo nào ?</span>
                            </div>        
                            <div class="camnang">
                                <a href="#">Cẩm nang du lịch</a>
                            </div>
                            <div class="ngaydangbai">
                                 <span>07/06/2019 | 05:15 &#9679; 2000 lượt xem </span>
                            </div>              
                        </div>
                    </div>
                    <div class="slider1" >         
                        <div>
                            <img src="image/anh1.jpg"> 
                            <div class="tieudenho">
                                <span>Thời gian nào thích hợp để du lịch Thái Lan ?</span>
                            </div>
                            <div class="noibat">
                                <a href="#">Nổi bật</a>
                            </div>
                            <div class="ngaydangbai2">
                                <span>20/12/2019 | 05:15 &#9679; 2000 lượt xem</span>
                            </div>       
                        </div>
                        <div>
                            <img src="image/anh2.jpg">
                            <div class="tieudenho">
                                <span>Thời gian nào thích hợp để du lịch Thái Lan ?</span>
                            </div>
                            <div class="noibat">
                                <a href="#">Nổi bật</a>
                            </div>
                            <div class="ngaydangbai2">
                                <span>20/12/2019 | 05:15 &#9679; 2000 lượt xem</span>
                            </div>       
                        </div>
                        <div>
                            <img src="image/anh3.jpg">
                            <div class="tieudenho">
                                <span>Thời gian nào thích hợp để du lịch Thái Lan ?</span>
                            </div>
                            <div class="noibat">
                                <a href="#">Nổi bật</a>
                            </div>
                            <div class="ngaydangbai2">
                                <span>20/12/2019 | 05:15 &#9679; 2000 lượt xem</span>
                            </div>       
                        </div>
                        <div>
                            <img src="image/anh2.jpg">
                            <div class="tieudenho">
                                <span>Thời gian nào thích hợp để du lịch Thái Lan ?</span>
                            </div>
                            <div class="noibat">
                                <a href="#">Nổi bật</a>
                            </div>
                            <div class="ngaydangbai2">
                                <span>20/12/2019 | 05:15 &#9679; 2000 lượt xem</span>
                            </div>       
                        </div>
                    </div>  
                </div>
                <div class="col-lg-5">
                    <div class="scrollbar" id="style-1">
                        <div class="force-overflow">
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div> 
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div>      
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div>      
                            <div class="huongdan">
                                <div class="d-flex">
                                    <div class="dulich2">
                                        <img src="image/dulich1.jpg">
                                    </div>
                                    <div class="kndl">
                                        <div class="dieucanbiet">
                                            <a href="#">Những điều cần biết về du lịch Thái Lan 2019</a>
                                        </div>
                                        <div class="kn-dl">
                                            <a href="#">Kinh nghiệm du lịch</a>
                                        </div>
                                        <div class="ngaydang">
                                            20/12/2019 | 05:15 &#9679; 1234 lượt xem 
                                        </div> 
                                    </div>
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="timhieuthem">
                        <a href="#">
                            <button type="button" class="btn">
                                Xem thêm ->
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script >
            $('.slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade:true,
            asNavFor: '.slider1'
            });
            $('.slider1').slick({
            nextArrow: '<button class="slick-next"><img src="image/right.png"></button>',
            prevArrow: '<button class="slick-prev"><img src="image/left.png"></button>',
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider',
            dots: true,
            focusOnSelect: true,
            });
    </script> 
    <div class="container">
       <div class="row">
           <div class="col-lg-6">
                <div class="hottour">
                    <span>THƯ VIỆN</span>
                    <div class="Underline1"></div>
                    <div class="Underline2">
                        <div class="diemdennoibat">Video</div>
                    </div>
                </div> 
           </div>
           <div class="col-lg-6">
               <div class="continue">
                   <a href="#">Xem thêm</a>
               </div>
           </div>
       </div>
    </div>
    <section class="Video">
       <div class="slider2">
           <div>
               <img src="image/video1.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
            </div>
           <div>
               <img src="image/video2.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
           <div>
               <img src="image/video3.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
           <div>
               <img src="image/video4.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
           <div>
               <img src="image/video4.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
           <div>
               <img src="image/video4.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
           <div>
               <img src="image/video2.jpg">
               <p>Điểm tham quan Ibaraki mà du khách có thể trải nghiệm quanh năm!</p>
           </div>
       </div> 
    </section>
    <script>
        $('.slider2').slick({
        nextArrow:'<button class="slick-right"><img src="image/right1.png"></button>',
        prevArrow: '<button class="slick-left"><img src="image/left1.png"></button>',
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        dots:true,
        });
    </script>  
    <div class="container">
       <div class="row">
           <div class="col-lg-6">
                    <div class="Underline2">
                        <div class="diemdennoibat" style="font-weight: bold;top: 0px">
                            HÌNH ẢNH
                        </div>
                    </div>
           </div>
           <div class="col-lg-6">
               <div class="continue1">
                   <a href="#">Xem thêm</a>
               </div>
           </div>
       </div>     
    </div>    
    <section class="menu7">
        <div class="chia-hinh-anh">
           <div class="Image">   
                <img src="image/image1.jpg">           
                <p>Xem chung kết U23 Châu Á cùng Hanoitourist</p>
          </div>
           <div class="Image">   
                <img src="image/image2.jpg">          
                <p>Getting Cheap Airfare For Last Minute Travel</p>
          </div>
           <div class="Image">   
              <img src="image/image3.jpg">
                <p>Mother Earth Hosts Our Travels</p>
          </div>
           <div class="Image">   
              <img src="image/image4.jpg">
                  <p>Lost In Lagos Portugal</p>
          </div>
           <div class="Image">   
              <img src="image/image4.jpg">
                <p>Lost In Lagos Portugal</p>
          </div>
           <div class="Image">   
              <img src="image/image5.jpg">
                <p>Les Houches The Hidden Gem Of The Chamonix Valley</p>
          </div>
           <div class="Image">   
              <img src="image/image6.jpg">
                  <p>Why Las Vegas Hotel Rooms For You</p>
          </div>
           <div class="Image">   
              <img src="image/image7.jpg">
                <p>The Luxury Of Traveling With Yacht Charter Companies</p>
          </div>
           <div class="Image">   
              <img src="image/image8.jpg">
                  <p>Get Around Easily With A New York Limousine Service</p>
          </div>
           <div class="Image">   
              <img src="image/image8.jpg">
                <p>Get Around Easily With A New York Limousine Service</p>
          </div>
        </div>
        </div>
    </section>
    <section class="banner-home">
        <img src="image/banner-home.jpg">
    </section>
    <section class="Doitac">
        <div class="doitac">
            <div class="container">
                <div class="col-lg-6">
                    <div class="doitaccuachungtoi">
                        <span>ĐỐI TÁC CỦA CHÚNG TÔI</span>
                    </div> 
                </div>
            </div>
            <div class="hangdoitac">
                <div class="slider3">
                    <div class="slider-item">
                        <a href="#"><img src="image/vietnam-airlines.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/bidv.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/vietjet.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/vp-bank.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/vietcombank.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/jetstar.jpg"></a>
                    </div>
                     <div class="slider-item">
                        <a href="#"><img src="image/jetstar.jpg"></a>
                    </div>
                    <div class="slider-item">
                        <a href="#"><img src="image/bidv.jpg"></a>
                    </div>
                </div>          
            </div>
                <script>
                    $('.slider3').slick({
                    nextArrow:'<button class="slick-right1"><img src="image/right1.png"></button>',
                    prevArrow:'<button class="slick-left1"><img src="image/left1.png"></button>',
                    infinite: true,
                    slidesToShow: 6,
                    slidesToScroll: 3,
                    dots:true,
                    });
                </script>  
            </div>
        </div>
    </section>
    <section class="Dangky"> 
            <div class="dangky">
                <div class="nhanthongtin d-flex">
                    <a href="#"><img src="image/email.png"></a>
                    <p>ĐĂNG KÝ NHẬN THÔNG TIN KHUYẾN MẠI</p>
                </div>
                <div class="dienthongtin">
                    <p>Họ và tên</p>
                    <input type="text" name="nhap" placeholder="Nhập họ và tên của bạn">
                </div>
                <div class="dienthongtin">
                    <p>Email</p>
                    <input type="text" name="nhap" placeholder="Nhập email của bạn">
                </div>
                <div class="dienthongtin">
                    <p>Số điện thoại</p>
                    <input type="text" name="nhap" placeholder="Nhập số điện thoại của bạn">
                </div>
                <div class="xacnhan">
                    <button type="button" class="btn">ĐĂNG KÝ</button>
                </div> 
            </div>   
    </section> 
    <section class="sukien">
        <div>
            <img src="image/sukien.jpg">
        </div>
          <div>
            <img src="image/khampha.jpg">
        </div>
         <div>
            <img src="image/sukien.jpg">
        </div>   
    </section>
    <div class="hanoitourists">
       <div class="container">
            <div class="row">
               <div class="col-lg-3">
                    <div class="title">
                        <h2>TOUR TRONG NƯỚC</h2>
                    </div> 
                    <div class="list">
                        <ul>
                            <li><span><a href="#">Tour theo điểm đến</a></span></li>
                            <li><span><a href="#">Tour theo dịp</a></span></li>
                            <li><span><a href="#">Tour thường xuyên</a></span></li>
                            <li><span><a href="#">Hot tour</a></span></li>
                            <li><span><a href="#">Combo du lịch</a></span></li>
                        </ul> 
                    </div>                          
               </div>
               <div class="col-lg-3">
                   <div class="title">
                        <h2>TOUR NƯỚC NGOÀI</h2>
                    </div> 
                    <div class="list">
                        <ul>
                            <li><span><a href="#">Tour theo điểm đến</a></span></li>
                            <li><span><a href="#">Tour thường xuyên</a></span></li>
                            <li><span><a href="#">Hot tour</a></span></li>
                            <li><span><a href="#">Combo du lịch</a></span></li>
                        </ul> 
                    </div>                      
               </div>
               <div class="col-lg-3">
                   <div class="title">
                        <h2>DỊCH VỤ</h2>
                    </div> 
                    <div class="list">
                        <ul>
                            <li><span><a href="#">Visa</a></span></li>
                            <li><span><a href="#">Du thuyền</a></span></li>
                            <li><span><a href="#">Khách sạn</a></span></li>
                            <li><span><a href="#">Vé máy bay</a></span></li>
                        </ul> 
                    </div>                      
               </div>
               <div class="col-lg-3">
                   <div class="title">
                        <h2>VỀ HANOITOURIST</h2>
                    </div> 
                    <div class="list">
                        <ul>
                            <li><span><a href="#">Giới thiệu</a></span></li>
                            <li><span><a href="#">Liên hệ</a></span></li>
                            <li><span><a href="#">Tin tức</a></span></li>
                            <li><span><a href="#">Thư viện</a></span></li>
                        </ul> 
                    </div>                      
               </div>
            </div>
            <div class="Underline4">
               <hr>
            </div>
            <div class="row">
                <div class="col-lg-12">                   
                    <a href="#"><img src="image/logo-hnt.jpg"></a>
                </div>
            </div>
            <div class="hnt">
               <p>CÔNG TY LỮ HÀNH HANOITOURIST - TỔNG CÔNG TY DU LỊCH HÀ NỘI</p>
            </div>
            <div class="row">
               <div class="col-lg-6">
                    <div class="lienhe">
                       <img src="image/gps.png">
                       <span>
                        Trụ sở chính: 18 Lý Thường Kiệt, P. Phan Chu Trinh, Q. Hoàn Kiếm, Hà Nội.
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/gps.png">
                       <span>
                        Văn phòng giao dịch: Số 134 Nguyễn Thái Học, P. Điện Biên, Q. Ba Đình, Hà Nội.
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/web.png">
                       <span>
                        Website: www.hanoitourist.vn
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/mail.png">
                       <span>
                        Email: info@hanoitourist-travel.com 
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/fax.png">
                       <span>
                        Fax: 024 6260 6777
                       </span>
                    </div>
               </div>
                <div class="col-lg-6">
                    <div class="lienhe">
                       <img src="image/1.png">
                       <span>
                        Phòng Du Lịch Nước Ngoài 1: 02438.266.715 - 02438.256.036 - 0988.083.129
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/1.png">
                       <span>
                       Phòng Du Lịch Nước Ngoài 2: 02462.703.307 - 091.336.1136
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/1.png">
                       <span>
                        Phòng Du Lịch Nội Địa: 024.6260.6886 - ‎0904.929.252
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/1.png">
                       <span>
                        Phòng Giao Dịch Q.Ba Đình: 0243.‎936.4979 - Hotline: 0904.555.879
                       </span>
                    </div>
                    <div class="lienhe">
                       <img src="image/web.png">
                       <span>
                            Hệ Thống Website: Du Lịch Hàn Quốc <span style="color: #ffbf05">&#8226;</span> Du Lịch Nhật Bản <span style="color: #ffbf05">&#8226;</span> Du Lịch Châu Âu
                        </span>
                    </div>
               </div>
            </div>
            <div class="Underline4">
                <hr>
            </div>
            <div class="row">
                <div class="col-lg-3">              
                    <div class="baocao">
                        <img src="image/bocongthuong.png">
                    </div>   
                </div>
                <div class="col-lg-3">              
                    <div class="baocao">
                        <img src="image/dadangky.png">
                    </div>   
                </div>
                <div class="col-lg-6">
                     <div class="linklienhe">
                        <span>Liên hệ cộng đồng:</span>
                        <img src="image/instagram.png">
                        <img src="image/fb.png">
                        <img src="image/ytb.png">
                        <img src="image/twitter.png">
                    </div>                              
                </div>
            </div>         
            <div class="thanhtoan">
                <span>PHƯƠNG THỨC THANH TOÁN</span>  
            </div>
            <div class="d-flex">
                <div class="phuongthuc">
                    <img src="image/123-pay.png">
                </div> 
                <div class="phuongthuc">
                    <img src="image/nganluong.png">
                </div> 
                 <div class="phuongthuc">
                    <img src="image/visa.png">
                </div> 
                 <div class="phuongthuc">
                    <img src="image/mastercard.png">
                </div> 
                 <div class="phuongthuc">
                    <img src="image/jcb.png">
                </div> 
                 <div class="phuongthuc">
                    <img src="image/visa1.png">
                </div> 
                 <div class="phuongthuc">
                    <img src="image/mcsc.png">
                </div> 
            </div>      
        </div>
        <div class="footter">
            <span>Design by Az Solutions</span>
        </div>
    </div>
</body>
</html>                         